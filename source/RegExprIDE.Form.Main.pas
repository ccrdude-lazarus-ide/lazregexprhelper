{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Form that allows evaluation/testing of regular expressions.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2022 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit RegExprIDE.Form.Main;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   EditBtn,
   ActnList,
   Grids,
   ButtonPanel,
   Menus,
   ExtCtrls;

type
   TResultMatchNumbers = array of integer;

   TResultMatch = record
      Positions: TResultMatchNumbers;
      Lengths: TResultMatchNumbers;
   end;
   TResultMatches = array of TResultMatch;

   { TFormRegExprTests }

   TFormRegExprTests = class(TForm)
      aTemplatePascalClassDefinition: TAction;
      aTemplateSimpleNumbers: TAction;
      aEvaluate: TAction;
      alRegExpr: TActionList;
      bnBottom: TButtonPanel;
      cbModifierM: TCheckBox;
      cbModifierI: TCheckBox;
      cbModifierX: TCheckBox;
      cbModifierR: TCheckBox;
      cbModifierS: TCheckBox;
      cbModifierG: TCheckBox;
      editExpression: TEditButton;
      FlowPanel1: TFlowPanel;
      ilRegExpr: TImageList;
      labelExpression: TLabel;
      labelExpressionError: TLabel;
      labelInput: TLabel;
      labelResults: TLabel;
      miTemplatePascalClassDefinition: TMenuItem;
      menuRegExpr: TMainMenu;
      memoInput: TMemo;
      miTemplateSimpleNumbers: TMenuItem;
      miTemplates: TMenuItem;
      sgResults: TStringGrid;
      procedure aEvaluateExecute({%H-}ASender: TObject);
      procedure aTemplatePascalClassDefinitionExecute({%H-}Sender: TObject);
      procedure aTemplateSimpleNumbersExecute({%H-}ASender: TObject);
      procedure cbModifierIChange({%H-}Sender: TObject);
      procedure editExpressionButtonClick({%H-}ASender: TObject);
      procedure editExpressionKeyUp({%H-}ASender: TObject; var {%H-}AKey: word; {%H-}AShift: TShiftState);
      procedure FormShow({%H-}ASender: TObject);
      procedure sgResultsSelectCell({%H-}ASender: TObject; ACol, ARow: integer; var {%H-}ACanSelect: boolean);
   private
     FExpression: string;
      { private declarations }
      FMatches: TResultMatches;
      procedure UseSimpleTemplate(AExpression, ATestText: string);
      procedure ResizeByResultsTable;
      procedure Evaluate;
   public
      { public declarations }
      property Expression: string read FExpression write FExpression;
   end;

implementation

{$R *.lfm}

uses
   RegExpr,
   Clipbrd;

{ TFormRegExprTests }

procedure TFormRegExprTests.aEvaluateExecute(ASender: TObject);
begin
   Evaluate;
end;

procedure TFormRegExprTests.aTemplatePascalClassDefinitionExecute(Sender: TObject);
begin
   UseSimpleTemplate('([\w<>,$]*)\s*=\s*(class|record|interface)\s*(\(\s*[\w<>,$]*\s*\)|\s*of\s*\w*|abstract|)\s',
      '   TResultMatch = record'#13#10'   TFormRegExprTests = class(TForm)'#13#10'   a: TAction');
end;

procedure TFormRegExprTests.aTemplateSimpleNumbersExecute(ASender: TObject);
begin
   UseSimpleTemplate('([0-9]+)', 'This is 3 lines, consisting of 5 to 500 words.'#13#10'This is the second line. 78!'#13#10'1 + 2 = 3.');
end;

procedure TFormRegExprTests.cbModifierIChange(Sender: TObject);
begin
   Evaluate;
end;

procedure TFormRegExprTests.editExpressionButtonClick(ASender: TObject);
begin
   Evaluate;
end;

procedure TFormRegExprTests.editExpressionKeyUp(ASender: TObject; var AKey: word; AShift: TShiftState);
begin
   Evaluate;
end;

procedure TFormRegExprTests.FormShow(ASender: TObject);
begin
   editExpression.Text := FExpression;
   cbModifierI.Checked := RegExprModifierI;
   cbModifierR.Checked := RegExprModifierR;
   cbModifierS.Checked := RegExprModifierS;
   cbModifierG.Checked := RegExprModifierG;
   cbModifierM.Checked := RegExprModifierM;
   cbModifierX.Checked := RegExprModifierX;
   Evaluate;
end;

procedure TFormRegExprTests.sgResultsSelectCell(ASender: TObject; ACol, ARow: integer; var ACanSelect: boolean);
var
   m: TResultMatch;
begin
   if (ACol < 0) or (ARow < 0) then begin
      Exit;
   end;
   if ARow < Length(FMatches) then begin
      m := FMatches[ARow];
      if ACol < Length(m.Positions) then begin
         memoInput.SelStart := m.Positions[ACol] - 1;
         memoInput.SelLength := m.Lengths[ACol];
      end;
   end;
end;

procedure TFormRegExprTests.UseSimpleTemplate(AExpression, ATestText: string);
begin
   memoInput.Lines.Text := ATestText;
   editExpression.Text := AExpression;
   Evaluate;
end;

procedure TFormRegExprTests.ResizeByResultsTable;
var
   iNewTableHeight, iHeightDiff: integer;
begin
   iNewTableHeight := sgResults.GridHeight + 4;
   iHeightDiff := (iNewTableHeight - sgResults.Height);
   if (iHeightDiff > 0) then begin
      if Self.Height + iHeightDiff < Screen.DesktopHeight then begin
         Self.Height := Self.Height + iHeightDiff;
      end;
   end else begin
      Self.Height := Self.Height + iHeightDiff;
   end;
end;

procedure TFormRegExprTests.Evaluate;
var
   r: TRegExpr;
   sl: TStringList;
   iRow, i: integer;
   bSubExpressions: boolean;
begin
   try
      r := TRegExpr.Create(editExpression.Text);
      r.ModifierI := cbModifierI.Checked;
      r.ModifierR := cbModifierR.Checked;
      r.ModifierS := cbModifierS.Checked;
      r.ModifierG := cbModifierG.Checked;
      r.ModifierM := cbModifierM.Checked;
      r.ModifierX := cbModifierX.Checked;
      labelExpressionError.Caption := ' ';
      try
         sgResults.Clear;
         bSubExpressions := False;
         if r.Exec(memoInput.Text) then begin
            iRow := 1;
            repeat
               SetLength(FMatches, Succ(iRow));
               SetLength(FMatches[iRow].Lengths, Succ(r.SubExprMatchCount));
               SetLength(FMatches[iRow].Positions, Succ(r.SubExprMatchCount));
               if r.SubExprMatchCount > 0 then begin
                  bSubExpressions := True;
               end;
               for i := 0 to r.SubExprMatchCount do begin
                  FMatches[iRow].Lengths[i] := r.MatchLen[i];
                  FMatches[iRow].Positions[i] := r.MatchPos[i];
               end;
               if sgResults.ColCount < Succ(r.SubExprMatchCount) then begin
                  sgResults.ColCount := Succ(r.SubExprMatchCount);
               end;
               sgResults.RowCount := Succ(iRow);
               for i := 0 to r.SubExprMatchCount do begin
                  sgResults.Cells[i, iRow] := r.Match[i];
               end;
               Inc(iRow);
            until not r.ExecNext;
            sl := TStringList.Create;
            try
               sgResults.FixedRows := 1;
               RegExprSubExpressions(editExpression.Text, sl, r.ModifierX);
               for i := 0 to Pred(sl.Count) do begin
                  if bSubExpressions then begin
                     sgResults.Cells[i + 1, 0] := sl[i];
                  end else begin
                     sgResults.Cells[i, 0] := sl[i];
                  end;
               end;
            finally
               sl.Free;
            end;
         end;
      finally
         r.Free;
      end;
      ResizeByResultsTable;
      sgResults.AutoSizeColumns;
   except
      on E: Exception do begin
         labelExpressionError.Caption := E.Message;
      end;
   end;
end;

end.
