{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Registers TRegExpr helper into Lazarus IDE.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2022 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit RegRegExprHelper;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   Controls,
   SysUtils,
   MenuIntf,
   SrcEditorIntf,
   RegExprIDE.Form.Main;

procedure Register;

implementation

type

   { TRegExprHelper }

   TRegExprHelper = class
   private
   class var FInstance: TRegExprHelper;
      class function Instance: TRegExprHelper;
   private
      procedure DoEvaluate({%H-}Sender: TObject);
   public
      class constructor Create;
      class destructor Destroy;
   public
      procedure CreateMainMenuSubMenu();
   end;

procedure Register;
begin
   TRegExprHelper.Instance.CreateMainMenuSubMenu();
end;

{ TRegExprHelper }

class function TRegExprHelper.Instance: TRegExprHelper;
begin
   if not Assigned(FInstance) then begin
      FInstance := TRegExprHelper.Create;
   end;
   Result := FInstance;
end;

procedure TRegExprHelper.DoEvaluate(Sender: TObject);
var
   f: TFormRegExprTests;
   edit: TSourceEditorInterface;
   iResult: integer;
begin
   edit := SourceEditorManagerIntf.ActiveEditor;
   f := TFormRegExprTests.Create(nil);
   try
      if Assigned(edit) then begin
         f.Expression := edit.Selection;
      end;
      iResult := f.ShowModal;
      case iResult of
         mrOk:
         begin
            if Assigned(edit) then begin
               edit.Selection := f.editExpression.Text;
            end;
         end;
      end;
   finally
      f.Free;
   end;
end;

class constructor TRegExprHelper.Create;
begin
   FInstance := nil;
end;

class destructor TRegExprHelper.Destroy;
begin
   FInstance.Free;
end;

procedure TRegExprHelper.CreateMainMenuSubMenu;
begin
   RegisterIDEMenuCommand(mnuSource, 'TRegExprEvaluate', 'Evaluate regular expression...', @DoEvaluate);
end;

end.
