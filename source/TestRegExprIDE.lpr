{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-23  --  ---  Renamed from ram TestRegExprIDE to TestRegExprIDE
// 2017-05-23  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program TestRegExprIDE;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
   cthreads, {$ENDIF} {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms,
   RegExprIDE.Form.Main { you can add units after this };

{$R *.res}

var
   FormRegExprTests: TFormRegExprTests;

begin
   RequireDerivedFormResource := True;
   Application.Initialize;
   Application.CreateForm(TFormRegExprTests, FormRegExprTests);
   Application.Run;
end.



